import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "Public",
      component: () => import("../views/layouts/DefaultLayout.vue"),
      redirection: "/",
      children: [
        {
          path: "/",
          name: "home",
          component: () => import("../views/HomeView.vue"),
          redirection: "/",
        },
      ],
    },
    {
      path: "/dashboard",
      name: "DashboardLayout",
      component: () => import("../views/layouts/DashboardLayout.vue"),
      redirection: "/admin",
      children: [
        {
          path: "/admin/dashboard",
          name: "dashboard",
          component: () => import("../views/DashboardView.vue"),
        },
      ],
    },
  ],
});

export default router;
